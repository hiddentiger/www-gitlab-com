---
layout: markdown_page
title: Get started with GitLab
description: How would you like to get started?
---

- [Sign up](https://gitlab.com/users/sign_in#register-pane) for a free GitLab.com account
- [Sign in](https://gitlab.com/users/sign_in) to GitLab.com
- [Explore open source](https://gitlab.com/explore) projects on GitLab.com without signing in
- [Install](/install/) GitLab Core, our free, self-managed (aka "on prem") package
- [Contribute to open source GitLab](https://about.gitlab.com/community/contribute/)
- [Get a free trial of GitLab.com Gold](https://about.gitlab.com/free-trial/#gitlab-com), our top tier SaaS subscription.
- [Get a free trial of GitLab Ultimate](https://about.gitlab.com/free-trial/), our top tier Self-managed (aka "on prem") subscription
- [Watch a demo](https://about.gitlab.com/demo/) of GitLab
- [Buy a license or licenses for GitLab.com](https://customers.gitlab.com/plans) Bronze, Silver, or Gold. [See pricing](https://about.gitlab.com/pricing/).
- [Buy a license or licenses for GitLab Self-managed](https://customers.gitlab.com/plans) Starter, Premium, or Ultimate. [See pricing](https://about.gitlab.com/pricing/#self-managed).
- [Connect with a salesperson](https://about.gitlab.com/sales/)
- [Connect with a support person](https://about.gitlab.com/support/)
