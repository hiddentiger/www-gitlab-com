---
layout: job_family_page
title: "Expense Specialist"
---

GitLab is looking for a highly motivated individual to join our Accounting team as an Expense specialist. The role will assist in reviewing and approving expense reports for all team members.  We expect you to demonstrate the ability to work in a fast-paced, both individually and as a part of a group, and thrive within a dynamic and rapidly changing environment.

The Expense Specialist  will serve as the primary contact for internal and external communications regarding all T&E programs and reimbursements.

#### Responsibilities

- Audit expense reports for all team members, comparing details and support submitted with GitLab Reimbursement policy and making an independent judgment on approval/denial of submitted expenses through Expensify (expense management system).
- Resolve issues directly with team members and/or their managers related to non-compliant expense reimbursement submissions.
- Compile, analyze, and report expense reimbursement data to assess accuracy, completeness, and conformance to standards and policies.
- Develop and improve processes and procedures on how to manage T&E
- Execute ad-hoc assignments/projects as necessary


#### Requirements

- Minimum 2-4 years related accounting/accounts payable/expense reporting experience
- Strong proficiency in Expensify or other similar Expense Management Tools  Proficient in Google sheet (pivot tables and vlookups, etc.).
- Understanding of VAT reclaim (Germany, United Kingdom, Netherlands)
- Intermediate proficiency with Google Suite.
- Excellent interpersonal and written communication skills.
- Strong attention to detail.
- Ability to handle conflict.
- Provide a sense of urgency about the work, and have the ability to keep matters confidential when necessary
