---
layout: markdown_page
title: "Marketing"
description: "GitLab Marketing Handbook: Social Media, Corporate Marketing, Lead Generation, BDR, Community Relations and Product Marketing"
twitter_image: '/images/tweets/handbook-marketing.png'
---

## Welcome to the GitLab Marketing Handbook
{: .no_toc}

The GitLab Marketing team includes multiple functional groups: Revenue Marketing, Community Relations, Corporate Marketing, Marketing Operations and Product Marketing.
{: .note}

----

## On this page
{: .no_toc}

- TOC
{:toc .toc-list-icons}

----

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Handbooks
{: #marketing-handbooks}

- [Blog]
- [Website]
- [Social Media Guidelines]
- [Revenue Marketing]
     - [Field Marketing]
     - [Digital Marketing Programs]
     - [Sales Development (XDR)]
          - [Inbound BDR]
          - [Outbound SDR]
- [Campaigns]
- [Marketing Operations]
- [Corporate Marketing]
     - [Content Marketing]
- [Community Relations]
   - [Community Advocacy]
   - [Code Contributor Program]
   - [Evangelist Program]
- [Product Marketing]
- [Marketing Career Development]

----

## <i class="fab fa-gitlab fa-fw icon-color font-awesome" aria-hidden="true"></i> GitLab Marketing Purpose

<br />

<div class="alert alert-purple center"><h3 class="purple"><strong>We <i class="fas fa-heart orange font-awesome" aria-hidden="true"></i> GitLab</strong></h3></div>

GitLab's vision is to become the single application for DevOps by becoming best-in-class in every DevOps software stage, from project planning and source code management to CI/CD, monitoring, and security so that organizations can break free from complex DevOps toolchains. The GitLab Marketing team is here to:

- Meet board approved company goals and KPIs.
- Support and celebrate the GitLab community of users and contributors.
- Enable anyone to contribute to our open source products.
- Evangelize the GitLab product in an authentic and helpful way.
- Help each other to achieve our individual and company OKRs and KPIs.

## Data informed decisions

At GitLab, we are committed to making data informed decisions.  To that end, we publish our [monthly metrics reports on GitLab's public YouTube Unfiltered channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/search?query=marketing+metrics).

## Integrated Campaigns

Marketing functional groups collaborate to produce Integrated Campaigns. An Integrated Campaign is a communication effort that includes several campaign tactics such as blog posts, emails, events, advertisements, content on about.gitlab.com, videos, case studies, whitepapers, surveys, social outreach, and webcasts. An Integrated Campaign will have a campaign theme that summarizes the message we are communicating to our market.

### Active integrated campaigns

- [Just Commit](/handbook/marketing/campaigns/#just-commit)
- [Competitive campaign](/handbook/marketing/campaigns/#competitive-campaign)

### Have a new campaign idea? [Make a suggestion](/handbook/marketing/campaigns)


## <i class="fas fa-tasks fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Project Management Guidelines

Marketing uses GitLab for agile project management including [groups](https://docs.gitlab.com/ee/user/group/), [projects](https://docs.gitlab.com/ee/user/project/), [epics](https://docs.gitlab.com/ee/user/group/epics/), [roadmaps](https://docs.gitlab.com/ee/user/group/roadmap/), [issues](https://docs.gitlab.com/ee/user/project/issues/), [labels](https://docs.gitlab.com/ee/user/project/labels.html), and [boards](https://docs.gitlab.com/ee/user/project/issue_board.html). Read through the documentation on each of these GitLab features if you are unfamilar.

### Groups and projects

1. The Marketing General Group houses all marketing projects.
1. Labels should exist at the group level so they can be used across projects.
1. The following are the approved marketing projects, CMO approval is needed to begin a new project.
  - [Product Marketing](https://gitlab.com/gitlab-com/marketing/product-marketing) (includes PMM, AR, Customer Case studies, etc.)
  - [Community Relations](https://gitlab.com/gitlab-com/marketing/community-relations)
  - [Digital Marketing Programs](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs)
  - [Corporate Marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing) (includes content, website, corp events, brand, etc.)
  - [Marketing Operations](https://gitlab.com/gitlab-com/marketing/marketing-operations)
  - [Field Marketing](https://gitlab.com/gitlab-com/marketing/field-marketing)
  - [XDR](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit#slide=id.g3092f78080_20_4)
1. Issues should only be logged in team project (i.e. do no use `marketing/general`)
1. Don't create groups with subgroups (use labels to segment workstreams within a team's project issue tracker.)


### Issues, Milestones, and Epics
1. Each issue represents a discrete unit of work with a deliverable. For example [1](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/10) [2](https://gitlab.com/gitlab-com/marketing/online-growth/issues/8) [3](https://gitlab.com/gitlab-com/marketing/general/issues/2574)
1. Every MR should have an issue so that it can be tracked on issue boards.
1. Milestones represent units of work to be completed within a specific time frame, sometimes referred to as sprints. They are comprised of multiple issues that share a common due date, and help break large projects into more manageable parts.
1. Epics represent projects that comprise multiple issues. (Don't use "meta" issues for this purpose. If you have have existing meta issue you can promote them to epics using the `/promote` slack command.)
  - Epics live at the group level (e.g. issue from multiple marketing projects can be added to an epic.)
  - Epics are labeled with a group label of the team that owns the epic.
1. The top 3-5 strategic initatives are tracked in epics using the `CMO` label. (Don't apply the CMO label to other epics.)
1. Roadmaps are used for time-based display of epics with a start and end date. (for example, events and time-based campaigns.)

### Boards and Labels
1. Each team has one or more boards to track ongoing workstreams.
1. Generally, create a board for each function. (For example PMM has boards for Sales Enablement, Analyst Relations, Customer Relations, etc.)
1. Each board uses a standard set of columns/labels so that folks can easily understand what is happening on another teams board.
1. The board labels use group labels with `status:` and one of four statuses. Status labels are used on both issues and MRs:
  - **Optional***:`Status:Plan` - work that is proposed, in an exploratory state. To exit the plan stage the work must be assigned to a DRI. The DRI accepts responsibility for the task by changing the label from `Status:Plan` to `Status:WIP` and creating an MR if appropriate. The plan status is optional, as issues that don't require formal planning can be opened and labeled `Status:WIP`.
  - `Status:WIP` - work in progress that has been accepted and assigned to a DRI. Work in this stage should not be merged. Issues and MRs should be prepended with `WIP:`. At GitLab we allow reviewers to start reviewing right away before work is complete. Use [MVCs](https://about.gitlab.com/handbook/values/#iteration): At any time, if the work is complete enough that merging would be better than what current exist the issue should be labeled with `status:review` and `WIP:` should be removed from the title.
  - **Optional***: `Status:Review` - work has been completed enough that it is ready for formal review and approval. Work that is approved can be either merged or scheduled. The review status is optional. Work that doesn't require review can simply be merged/closed.
  - **Optional**: `Status:Scheduled` - work that is complete but should be scheduled for a future date. The scheduled status is optional as not all work will need to be scheduled.
  - `closed` - when work is delivered the issue should be closed.
1. Don't duplicate status labels at the project level. Use group labels (at the Marketing Group level) as much as possible.

## <i class="fas fa-tasks fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Rapid Response Process

### Summary
There are times when a rapid response is needed to address M&A in the industry, competitive moves and other opportunities to [newsjack](https://blog.hubspot.com/blog/tabid/6307/bid/32983/the-inbound-marketer-s-complete-guide-to-newsjacking.aspx). In order to effectively respond, GitLab marketing needs to monitor, create and publish quickly. Additionally, GitLab marketing needs to support GitLab executives with content, data and soundbytes for interviews, blog-posts, etc.

### Process
If a rapid response opportunity arises, please alert the head of corporate marketing (or CMO if head of corporate marketing is unavailable) via slack or text message. Rapid response collaboration will happen in either the #competition channel (if it is competitive) or in the #highwire channel (if it is about other news). The head of corporate marketing will propose a recommendation on how to best proceed with an external company message and recruit the resources to accomplish the plan (this becomes the #1 priority for each resource, unless physically impossible or their manager provides a replacement). A template for a rapid response can be found [here](https://docs.google.com/document/d/19Kc-GZ5B7V_zOA46zF1SXoD5qfavqmCAnXFJKS7P3sI/edit)

The head of corporate marketing will assess the rapid response request within 1 HOUR (9amEST-6pmPST). Urgency will be assessed and will be determined to be 3 HOURS (ASAP) or 24 HOURS turnaround, and the action plan will be scoped accordingly - with a bias towards releasing an MVC as soon as possible, and iterating as more news becomes available. Any disagreements on urgency or action will be escalated immediately to the CMO for a final decision.

While each response will be a custom plan, the rapid response team will leverage appropriate resources to execute on the agreed upon plan. PMM/TMMs should be responsible for technical and product content and industry expertise and context, Content/Social will be responsible for writing and publishing, Web will be responsible for any non-blog web page changes needed, and Community will be responsible for monitoring and responding in public channels.

## <i class="fas fa-users fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Team Functional Groups
{: #groups}


### Sales Development (XDR)

Business Development Representatives (BDRs) focus on serving the needs of prospective customers' during the beginning of their buying process. When prospective customers have questions about GitLab, the BDRs assist them or connect them to a technical team member as needed. During the initial exploration if the prospective customer is interested in continuing their exploration of GitLab, BDRs will connect them to an Account Executive (AE) or Strategic Account Leader (SAL).

Sales Development Representatives (SDRs) contact people who work at large organizations to uncover or create early stage sales opportunities for GitLab SALs. Account researchers arm the SDR team with insights about the accounts they are prospecting into including contact discovery, understanding enterprise-wide initiatives that GitLab could assist with, and ensuring accurate data quality of accounts and contact in salesforce.com.

[Position Description](/job-families/marketing/business-development-representative/){:.btn .btn-purple-inv .extra-space}
[Handbook][Sales Development (XDR)]{:.btn .btn-purple .extra-space}

### Field Marketing
{: .no_toc}

Field marketers focus on understanding the specific needs of the geographic regions where GitLab operates. They manage marketing activities, such as events and sponsorships, tailored to the needs of the region where the activity takes place.

[Position Description](/job-families/marketing/field-marketing-manager){:.btn .btn-purple-inv}
[Handbook][Field Marketing]{:.btn .btn-purple}

### Marketing Operations
{: .no_toc}

Marketing operations focuses on enabling the GitLab marketing organization with marketing technology, process, enablement and insights. They are responsible for evaluating, deploying and administering marketing systems, documenting and improving administrative processes, and analyzing our marketing data to ensure marketers are held accountable to continuously improving their work. Marketing Operations owns the tech stack used by Marketing.

[Position Description](/job-families/marketing/marketing-operations-manager/){:.btn .btn-purple-inv}
[Handbook][Marketing Operations]{:.btn .btn-purple}

### Corporate Marketing

Corporate Marketing is responsible for PR/communications, the stewardship of the GitLab brand, corporate events and the company-level messaging/positioning. The team is the owner of about.gitlab.com and oversees the website strategy. Corporate Marketing creates global marketing materials and communications and supports the field marketing teams so they can execute regionally while staying true to the GitLab brand.

[Handbook][Corporate Marketing]{:.btn .btn-purple}

### Content Marketing
{: .no_toc}

Content marketers focus on understanding our audience of developers, IT ops practitioners, and IT leadership. They create useful content for GitLab's audiences, and ensure that the content is delivered to the right audience, at the right time, and in the right way.

[Position Description](/job-families/marketing/content-editor/){:.btn .btn-purple-inv}
[Handbook][Content Marketing]{:.btn .btn-purple}

### Digital Marketing Programs
{: .no_toc}

Digital Marketing Programs focuses on executing integrated campaigns, with a digital-first mindset. They are responsible for executing, deploying, and tracking emails and supporting both sales and marketing in outbound mass communications.

Digital Marketing Programs includes online marketing, focused on managing online advertising, website experiments, and search engine optimization (SEO). Online advertising is aimed at increasing the volume of relevant traffic to GitLab's marketing site, website experiments are focused on improving web traffic-to-form submission conversion, and SEO is aimed at ensuring our marketing site ranks for the search engine keywords our audiences care about.

[Position Description](/job-families/marketing/digital-marketing-programs-manager/){:.btn .btn-purple-inv}
[Handbook][Digital Marketing Programs]{:.btn .btn-purple}

### Marketing Program Management
{: .no_toc}

Marketing Program Managers are responsible for creating and managing business to business marketing programs, supporting Field events, driving TOFU/MOFU traffic and continued engagement with both prospects and customers.    

[Position Description](/job-families/marketing/marketing-program-manager/){:.btn .btn-purple-inv}
[Handbook][Marketing Programs]{:.btn .btn-purple}


### Community Relations

Community Relations includes community advocacy, code contributor program and evangelist program functions. The team is focused on answering the following questions:

- What are scalable developer education tools?
- How do we turn in person feedback at events into actionable product requests?
- What are the best and most engaging talks we can give to help educate developers?
- How do we support the community?
- How do we make the documentation even better?
- How do we make it even more fun and easy to get started?
- What is engaging developer content for blog, video, social media?
- How do we build our global meetup plan + make it easy to love GitLab?
- What is the developer GitLab experience?
- How do we use social media to support the community?
- How do field marketing and developer relations work together to support the community?

[Handbook][Community Relations]{:.btn .btn-purple}

### Product Marketing

Product marketing is GitLab's interface to the market. The market is made up of customers, analysts, press, thought leaders, competitors, etc. Product marketing enables other GitLab groups such as Sales, Marketing, and Channel with narrative, positioning, messaging, and go-to-market strategy to go outbound to the market. Product marketing does market research to gather customer knowledge, analyst views, market landscapes, and competitor intelligence providing marketing insights inbound to the rest of GitLab.

[Handbook][Product Marketing]{:.btn .btn-purple}

## <i class="fas fa-suitcase fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Production
{: #marketing-products}

<!-- The following HTML blocks are the Marketing Products boxes -->
<!-- DON'T EDIT THIS PART BELOW UNLESS YOU KNOW WHAT YOU'RE DOING :) -->
<div class="row mkt-row">
  <a href="/blog/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/blog.png" alt="GitLab Marketing - Demand Generation - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">BLOG</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="https://gitlab.myshopify.com/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/swag_shop.png" alt="GitLab Marketing - Design - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">SWAG</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="/blog/categories/events/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/location.png" alt="GitLab Marketing - Developer Relations - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">EVENTS</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
</div>
<!-- NEXT ROLL OF 4 -->
<div class="row mkt-row">
  <a href="https://docs.gitlab.com/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/documentation.png" alt="GitLab Marketing - Demand Generation - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">DOCS</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/website.png" alt="GitLab Marketing - Design - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">WEBSITE</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="https://docs.gitlab.com/ee/university/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/training.png" alt="GitLab Marketing - Product Marketing - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">UNIVERSITY</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
</div>
<!-- END OF MARKETING PRODUCTS -->


## <i class="fas fa-cogs fa-fw icon-color font-awesome" aria-hidden="true"></i> Meetings and structure
{: #meetings}

These are just the required meetings for team members and managers. Of course, meetings are encouraged when it expedites a project or problem solving among members, so the team and company. Don't be afraid to say "Hey, can we hangout?" if you need help with something.

### Weekly Marketing Strategy and Tactics Call (All Marketing team members)
{: .no_toc}

**Meeting goal: This is the main meeting where marketing strategy and tactics are discussed. Everyone in marketing is invited, but attendance is optional.**

**Run time: 50 minutes**

The Marketing team meets weekly to review announcements, strategy developments, company updates, OKRs, and KPIs. This is the primary meeting for discussing strategy and tactics, resolving conflicts and communicating information.

The meeting should run as follows:

- Prior to the meeting, any marketer adds agenda items to the agenda doc, linked to the invitation.
- The CMO prioritizes the agenda and kicks off the meeting with announcements and updates on strategy, company and org.
- The remaining time is used for open discussion and Q&A.
- Interruption is encouraged! Team members are encouraged to interrupt and ask questions throughout.
- All team members are also encouraged to bring forward discussion topics that they want to share with the team or to have covered during the call.
- Action items are recorded and owners assigned. Owners are responsible to report back with the outcome of the action item.

### Weekly Direct Reports Meetings (Each manager with their direct reports)
{: .no_toc}

**Meeting goal: For managers to work with direct reports on personnel issues, PeopleOps requirements, performance reviews and hiring.**

**Run time: 25 minutes**

Strategy and tactics should be discussed in the weekly Marketing Strategy and Tactics meeting. Weekly direct reports meetings should be held to discuss tactical people management issues. These meetings should be short and tactical - save strategy discussions for the all-marketing weekly call.

The meeting should run as follows:

- Populate the agenda (linked to the meeting invite) prior to the meeting.
- Manager guides the team through the agenda.
- PeopleOps informs team of any upcoming requirements.

## <i class="far fa-thumbs-up fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing team SLAs (Service Level Agreements)
{: #sla}

When working remotely in a fast-paced organization, it is important for a team to agree on a few basic service level agreements on how we would like to work together. With any of these, things can come up that make it not possible to meet the SLAs, but we all agree to use best effort when possible.

- Respond to your emails by end of next business day.
- Respond when you are cc'd with an action item on issues by end of next business day.
- Be on time to meetings. We start at on time.
- Acknowledge receipt of emails (community@, FYIs) by BCC'ing the list.
- Try not to email co-workers on weekends. Time off is important. We all have stressful weeks so please unplug on the weekends where possible.

## <i class="far fa-file-code fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Handbook Updates
{: #handbook}

Anything that is a process in marketing should be documented in the Marketing Handbook.

- Format of all pages should be as follows:
    - Welcome to the Handbook.
    - Functional group overview if handbook for entire functional group or organization.
    - "On this page" index of all top level headers on the current page ([create a ToC]).
    - Links to other handbooks included on this page.
- Rather than create many nested pages, include everything on one page of your role's handbook with an index at the top.
- Each role should have a handbook page.
- If more than one person are performing a role, the task should be shared to update the handbook with all processes or guidelines.
- Follow the [Markdown Style Guide] for about.GitLab.com.

## <i class="fas fa-rocket fa-fw icon-color font-awesome" aria-hidden="true"></i> How to contact marketing
{: #contact-marketing}

- [**GitLab Marketing public issue tracker**](https://gitlab.com/gitlab-com/marketing); please use confidential issues for topics that should only be visible to team members at GitLab
- You can also send an email to the Marketing team (see the "Email, Slack, and GitLab Groups and Aliases" Google doc for the alias).
- [**Chat channel**](https://gitlab.slack.com/archives/marketing); please use the `#marketing` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.

### Requests from other teams

#### Social ([@GitLab](https://twitter.com/gitlab) only, [@GitLabstatus](https://twitter.com/gitlabstatus) is managed by [Infrastructure](/handbook/engineering/infrastructure/))

Everyone posts their own social updates, to the extent possible. If you want to request that something in one of these categories be posted, reach out to the point person below. They reserve the right to say no to your request, and copy in all of these categories may be adjusted by a marketing team member to ensure consistency in our brand voice.

- Events: Jr. Content Editor
- Release & technical posts/product updates: Technical writer
- User questions/comments on Twitter: Community Advocates
- Leadgen campaigns: Content team
- UX Design: UX Lead
- People Ops: Jr. Content Editor
- Press coverage: Post in #marketing for assistance
- RTs of mentions: Post in #twitter
- CEO statement/posts: Post in #twitter

#### Blog post editing

- Product release posts: Product team
- Technical community posts/tutorials: Incoming technical editor
- CEO statements/updates: CMO

   - Plan on some delay when you pitch, so think about whether your post will still be relevant in one month or more.
   - If you want a blog post to be published, you should be prepared to write it and format it independently before expecting a review. An easy way to do this is to copy the latest blog post file and edit it, filling in all the fields with your information and post text. Be sure to add a public domain/creative commons cover image, and [attribute properly](/handbook/marketing/blog/#cover-image) at the bottom of the post.

#### Newsletter

**Marketing Newsletter**

Marketing sends out a bi-weekly newsletter to our [newsletter subscribers](/handbook/marketing/marketing-operations/#email-segments) on the 10th and 25th of each month.

To add a content suggestion please find the Newsletter issue in the Marketing project and add your suggestion(s) as comment(s) to the issue in the format outlined on the issue description ( [See example](https://gitlab.com/gitlab-com/marketing/content-marketing/issues/142) ). The title of the Newsletter issue will be formatted as follows: `Newsletter MM/DD`.

Content suggestions can be submitted up to 5 business days before the send date to ensure there is enough time for Content and Marketing Programs Manager's (MPM) review and set up workflow.

Anyone in the company can add suggestions, but the Marketing Program Manager in charge of the newsletter program ([see division of MPM duties](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/#responsibilities)) will determine the final content of the newsletter. As a rule, we try to have a good balance of content, with a variety of blog posts, webcast landing pages, and product/company updates. The newsletter on the 25th will always lead with the release post for that month.

**Other Newsletter**

To request a newsletter to be sent to an audience outside the [newsletter subscribers](/handbook/marketing/marketing-operations/#email-segments) , please create an issue in the [Digital Marketing Programs project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues), using the `Newsletter` issue template.

Newsletter requests should be submitted no less than 5 business days before the intended send date to ensure there is enough time for Content and Marketing Programs Manager's (MPM) review and set up workflow.

#### Sponsorship

We are happy to sponsor events and meet-ups where a marketing benefit exists, subject to approval by Field Marketing Managers. These sponsorships may be in cash or in kind, depending on individual circumstances.

Organizational or project sponsorships may also be considered where a marketing benefit exists. Typically, these sponsorships will be in kind - e.g., developer time commitments, or [subsidized / free GitLab licenses](/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/).

Cash sponsorship of projects or organizations may be considered only in exceptional cases - for example, if a project or organization that GitLab depends on is struggling to survive financially.


### Chat Marketing channels
{: #chat}

We use our chat internally as a communication tool. The Marketing channels are as follows:

- `#marketing`: General marketing channel. Don't know where to ask a question? Start here.
- `#advocate-for-a-day`: Community Advocate channel when other people are covering
- `#bdr-team`: For the Inbound BDR team
- `#cfp`: All call for speakers will be posted here.
- `#content`: Questions about blog posts, webcasts, the newsletter or other marketing content? This is the place to ask.
- `#devrel`: A channel for the developer relations team to collaborate.
- `#docs`: Technical writing and documentation questions? This is your room.
- `#events`: Everything you want to know about events.
- `#gitlab-pages`: Where we discuss everything related to GitLab Pages.
- `#lead-questions`: If there is a question regarding lead routing, scoring, etc.
- `#marketing-design`: Discuss, feedback, and share ideas on Marketing Design here.
- `#marketing-programs`: Discuss, ask questions, stay up-to-date on campaigns and events that are being organized by the Marketing Program Managers
- `#marketo-users`: Having issues with Marketo? Ask here first.
- `#mktgops`: Marketing Ops communication channel for questions and project updates
- `#outreach`: Having issues with Outreach? Ask here first.
- `#product-marketing`: Discuss, feedback related to product news, features and vision
- `#social`: Twitter, Facebook, and other social media questions?
- `#sdr-conversations`: place for XDR team brainstorm & sharing of ideas
- `#sdrs_and_bdrs`: For the XDR team - both inbound and outbound
- `#support`: Ask any and all technical questions here for a quick response.
- `#sfdc-users`: Having issues with SFDC? Ask here first.
- `#swag`: Request or question regarding swag.
- `#twitter`: Use this channel to suggest we tweet about something.
- `#website`: Discuss topics related to website redesign project

### Marketing email alias list
{: #email}

- Analysts@ company domain: external email address for contacting Analyst Relations at GitLab. Replies are forwarded to Analyst Relations manager and Product Marketing Director
- Community@ company domain: external email address for sending confirmation emails related to GitLab products. Replies are forwarded to Zen Desk support
- Content@ company domain: external email address associated with management of our SlideShare account. Replies are forwarded to Content Marketing team and Marketing OPS Manager
- Events@ company domain: external email address for sending live, VIP &amp; in-person training related emails. Replies go to Field Marketing Managers and Marketing OPS Manager
- Giveaways@ company domain: external email address for receiving content & social media related promotional giveaways. Replies go to Content Marketing Team and Marketing OPS Manager
- Leads@ company domain: external email address for internal Lead alerts. Replies go to Marketing OPS Manager
- News@ company domain: external email address used to send newsletter. Replies go to Marketing OPS Manager and Manager, Content Marketing
- MPM@ company domain: external email address used to send direct generic requests to the Marketing Program Managers
- MarketingOPS@ company domain: external email address used to direct generic operational requests to Marketing OPS Manager
- MarketingSFDC@ company domain: external email address associated with management of Salesforce. Replies forward to Manager, Digital Marketing Programs; Field Marketing Manager; Product Marketing Manager; and Manager, Content Marketing
- SecurityAlerts@ company domain: external email address used to send security alerts. Replies go to Marketing OPS Manager
- Sponsorships@ company domain: external email address used to manage sponsor requests from community. Replies forward to Community Advocate Team
- Support@ company domain: external email address for sending Breaking Change and/or support related customer communications. Replies go to Zen Desk support
- Surveys@ company domain: external email address for sending the Developer Survey and/or related surveys. Replies go to Content Team and Product Marketing Manager
- Webcasts@ company domain: external email address for sending webcast related emails. Replies go to Marketing OPS Manager and Marketing Program Managers

<!-- IDENTIFIERS -->

[cmo]: /job-families/chief-marketing-officer/
[create a ToC]: /handbook/product/technical-writing/markdown-guide/#table-of-contents-toc
[Markdown Style Guide]: /handbook/product/technical-writing/markdown-guide/

<!-- HANDBOOKS -->

[Blog]: /handbook/marketing/blog/
[Website]: /handbook/marketing/website/
[Campaigns]: /handbook/marketing/campaigns/
[Content Marketing]: /handbook/marketing/corporate-marketing/content/
[Community Relations]: /handbook/marketing/community-relations/
[Community Advocacy]: /handbook/marketing/community-relations/community-advocacy/
[Code Contributor Program]: /handbook/marketing/community-relations/code-contributor-program/
[Evangelist Program]: /handbook/marketing/community-relations/evangelist-program/
[Corporate Marketing]: /handbook/marketing/corporate-marketing
[Design]: /handbook/marketing/corporate-marketing/#design
[Field Marketing]: /handbook/marketing/revenue-marketing/field-marketing/
[Revenue Marketing]: /handbook/marketing/revenue-marketing/
[Marketing Programs]: /handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/
[Marketing Operations]: /handbook/marketing/marketing-operations/
[Digital Marketing Programs]: /handbook/marketing/revenue-marketing/digital-marketing-programs/
[Partner Marketing]: /handbook/marketing/product-marketing/partner-marketing/
[Product Marketing]: /handbook/marketing/product-marketing/
[Business Operations]: /handbook/business-ops/
[Sales Development (XDR)]: /handbook/marketing/revenue-marketing/xdr/
[Inbound BDR]: /handbook/marketing/revenue-marketing/xdr/#how-to-bdr
[Outbound SDR]: /handbook/marketing/revenue-marketing/xdr/#how-to-sdr
[Social Marketing]: /handbook/marketing/corporate-marketing/social-marketing/#social-channels-and-audience-segmentation-
[Social Media Guidelines]: /handbook/marketing/social-media-guidelines/
[Swag]: /handbook/marketing/corporate-marketing#swag
[Events]: /handbook/marketing/corporate-marketing/#corporate-events
[Marketing Career Development]: /handbook/marketing/career-development

<!-- Marketing Team: GitLab.com Handle -->

[emily]: https://gitlab.com/emily
[erica]: https://gitlab.com/erica
[jjcordz]: https://gitlab.com/jjcordz
[lukebabb]: https://gitlab.com/lukebabb
[evhoffmann]: https://gitlab.com/evhoffmann
[rebecca]: https://gitlab.com/rebecca
[elsje]: https://gitlab.com/elsje
[jburton]: https://gitlab.com/jburton
[jbroussard]: https://gitlab.com/jbroussard
[williamchia]: https://gitlab.com/williamchia
[LaniceSims]: https://gitlab.com/LaniceSims
[hschuler]: https://gitlab.com/hschuler
[mhamilton]: https://gitlab.com/mhamilton
[Adam.Pestreich]: https://gitlab.com/Adam.Pestreich

<!-- EXTRA STYLES APPLIED FOR THIS PAGE ONLY -->

<style>
.purple {
  color: rgb(107,79,187) !important;
}
.orange {
  color:rgb(252,109,38) !important;
}
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
.btn-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-purple:hover {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv:hover {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-orange {
  color: rgb(252,109,38);
  background-color: #fff;
  border-color: rgb(226,67,41);
}
.btn-orange:hover {
  color: #fff;
  background-color: rgb(252,109,38);
  border-color: rgb(226,67,41);
}
.product.thumbnail img {
  display: block;
  max-width: 50%;
  margin: 20px auto;
}
.thumbnail img {
  display: block;
  max-width: 30%;
  margin: 20px auto;
}
.caption h4 {
  text-align: center;
}
.mkt-box {
  padding-bottom: 10px;
  padding-top: 10px;
  cursor: pointer;
}
.mkt-box:hover {
  /*border-radius: 5px;*/
  box-shadow:0 1px 5px rgba(0,0,0,0.3), 0 0 2px rgba(0,0,0,0.1) inset;
}
.mkt-row {
  padding-top: 20px;
  padding-bottom: 5px;
}
.mkt-row a:focus {
  outline: none;
}
.modal-header h2 {
  margin-top: 0;
}
.modal-footer p {
  margin-bottom: 0;
}
.center {
  text-align: center;
  display: block;
  margin-right: auto;
  margin-left: auto;
}
.description {
  color: #999;
}
.extra-space {
  margin-bottom: 5px;
}
.alert-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: rgba(107,79,187,.5);
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
ul.toc-list-icons li ul li i.slack {
  color: rgb(224,23,101);
}
ul.toc-list-icons li ul li i.email {
  color: rgb(192,0,0);
}
</style>
