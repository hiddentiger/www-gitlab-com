---
layout: markdown_page
title: "Personas Vision - Project managers"
---

- TOC
{:toc}

## Who are project managers?
TBD

## What's next & why
- [Test cases, test suites, and test sessions](https://gitlab.com/groups/gitlab-org/-/epics/617)

## Key Themes
- [Agile portfolio management](https://gitlab.com/groups/gitlab-org/-/epics/667)
- [Custom workflow per group](https://gitlab.com/groups/gitlab-org/-/epics/364)

## Stages with project management focus

There are several stages involved in developing the executive's toolbox at GitLab. These include, but are not necessarily limited to the following:
- [Plan](/direction/plan)
